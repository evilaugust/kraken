package catalog

import (
	"errors"
	"fmt"
	"net"
	"time"
)

var ErrConnRefused = errors.New("dial tcp4: connect: connection refused")

func AttemptTCPHealthCheck(addr string, timeout time.Duration) error {
	errConnRefused := fmt.Errorf("dial tcp4 %s: connect: connection refused", addr)
	dialer := net.Dialer{Timeout: timeout}

	conn, err := dialer.Dial("tcp4", addr)
	if err != nil {
		if err.Error() == errConnRefused.Error() {
			return ErrConnRefused
		}

		return err
	}

	return conn.Close()
}
