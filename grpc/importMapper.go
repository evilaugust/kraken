package grpc

import (
	"errors"
	"strings"

	"gitlab.com/cosmotek/kraken/catalog"
)

// ErrNotGRPCService is used by FromCatalog when a import fails
// due to invalid GRPC config.
var ErrNotGRPCService = errors.New("cannot convert catalog.Service to grpc.Service: config value 'ingress_type' must be 'grpc'")

// Service is a GRPC service with configuration options
// and metadata used to direct traffic from a proxy to the
// service itself.
type Service struct {
	catalog.Service

	GRPCWebEnabled              bool
	PublicGRPCReflectionEnabled bool

	// If no services are whitelisted, all not-blacklisted services
	// will be allowed in. If any services are whitelisted, only those
	// whitelisted will be allowed in.
	WhiteListedServices []string

	// If the whitelist is empty, all services except those in the
	// blacklist will be allowed in.
	BlackListedServices []string
}

// FromCatalog imports a catalog.Service as a grpc.Service
// using the config provided on the catalog.Service struct.
// If the value 'ingress_type' is not 'grpc', an ErrNotGRPCService
// will be returned.
func FromCatalog(service catalog.Service) (Service, error) {
	val, ok := service.Config["ingress_type"]
	if !ok || val != "grpc" {
		return Service{}, ErrNotGRPCService
	}

	val, ok = service.Config["ingress_grpc_enable_web_wrapper"]
	if ok {
		val = strings.TrimSpace(strings.ToLower(val))
	}

	grpcWebEnabled := ok && (val == "1" || val == "yes" || val == "true")

	val, ok = service.Config["ingress_grpc_whitelist_services"]
	whitelistedServices := make([]string, 0)

	if ok {
		val = strings.TrimSpace(val)
		rawWhitelistedServices := strings.Split(val, ",")

		for _, svc := range rawWhitelistedServices {
			whitelistedServices = append(whitelistedServices, strings.TrimSpace(svc))
		}
	}

	val, ok = service.Config["ingress_grpc_blacklist_services"]
	blacklistedServices := make([]string, 0)

	if ok {
		val = strings.TrimSpace(val)
		rawBlacklistedServices := strings.Split(val, ",")

		for _, svc := range rawBlacklistedServices {
			blacklistedServices = append(blacklistedServices, strings.TrimSpace(svc))
		}
	}

	return Service{
		Service:             service,
		GRPCWebEnabled:      grpcWebEnabled,
		WhiteListedServices: whitelistedServices,
		BlackListedServices: blacklistedServices,
	}, nil
}
