package grpc

import (
	"context"
	"crypto/md5"
	"encoding/json"
	"errors"
	"math/rand"
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/rs/cors"
	"github.com/rs/zerolog"
	"gitlab.com/cosmotek/kraken/catalog"
	"gitlab.com/cosmotek/kraken/grpc/reflector"
	"gitlab.com/cosmotek/kraken/metrics"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"github.com/vgough/grpc-proxy/proxy"

	"goji.io"
	"goji.io/pat"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	_ "net/http/pprof"

	_ "golang.org/x/net/trace"
)

var ErrDone = errors.New("loop completed, exiting.")

func init() {
	grpc.EnableTracing = true
}

func contains(in []string, cmp string) bool {
	for _, item := range in {
		if item == cmp {
			return true
		}
	}

	return false
}

type GRPCProxy struct {
	NamedServiceResolver NamedServiceResolver
	Logger               zerolog.Logger
	Collector            *metrics.Collector

	TLSEnabled  bool
	TLSKeyFile  string
	TLSCertFile string
}

func (g *GRPCProxy) ServeGRPC(addr string) error {
	logger := g.Logger.With().Str("mod", "consul_nsr").Logger()
	options := make([]grpc.ServerOption, 0)

	if g.TLSEnabled {
		tlsCreds, err := credentials.NewServerTLSFromFile(g.TLSCertFile, g.TLSKeyFile)
		if err != nil {
			logger.Fatal().Err(err).Str("key_file", g.TLSKeyFile).Str("cert_file", g.TLSCertFile).Msg("failed to create server tls")
		}

		options = append(options, grpc.Creds(tlsCreds))
	}

	options = append(options, grpc.CustomCodec(proxy.Codec()))
	options = append(options, grpc.UnknownServiceHandler(proxy.TransparentHandler(&Director{g.NamedServiceResolver, g.Logger, g.Collector})))

	server := grpc.NewServer(options...)
	listener, err := net.Listen("tcp4", addr)
	if err != nil {
		return err
	}

	logger.Info().Str("addr", addr).Msg("starting grpc endpoint")
	return server.Serve(listener)
}

func (g *GRPCProxy) ServeGRPCWeb(addr string, allowedOrigins []string, debug bool) error {
	logger := g.Logger.With().Str("mod", "consul_nsr").Logger()

	options := make([]grpc.ServerOption, 0)
	options = append(options, grpc.MaxConcurrentStreams(1000))
	options = append(options, grpc.ConnectionTimeout(time.Second*10))

	// options = append(options, grpc.MaxRecvMsgSize(10*1024*1024))
	options = append(options, grpc.MaxSendMsgSize(10*1024*1024))

	options = append(options, grpc.CustomCodec(proxy.Codec()))
	options = append(options, grpc.UnknownServiceHandler(proxy.TransparentHandler(&Director{g.NamedServiceResolver, g.Logger, g.Collector})))

	gserver := grpc.NewServer(options...)
	mux := goji.NewMux()

	mux.HandleFunc(pat.New("/*"), func(res http.ResponseWriter, req *http.Request) {
		// StatusOK required for LB healthchecks
		http.Error(res, "This GPRC-Web endpoint doesn't support standard HTTP traffic", http.StatusOK)
	})

	server := grpcweb.WrapServer(gserver)
	handler := http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		if server.IsGrpcWebRequest(req) {
			server.ServeHTTP(res, req)
			return
		}

		mux.ServeHTTP(res, req)
	})

	c := cors.New(cors.Options{
		AllowedOrigins:   allowedOrigins,
		AllowCredentials: true,
		AllowedMethods:   []string{"OPTIONS", "HEAD", "GET", "POST", "PUT", "DELETE"},
		AllowedHeaders:   []string{"*"},
		Debug:            debug,
	})

	logger.Info().Str("addr", addr).Msg("starting grpc-web endpoint")
	if g.TLSEnabled {
		return http.ListenAndServeTLS(addr, g.TLSCertFile, g.TLSKeyFile, c.Handler(handler))
	}

	return http.ListenAndServe(addr, c.Handler(handler))
}

type NamedServiceResolver interface {
	Resolve(string) (string, bool)
	Remove(string, string)
}

type ConsulNSR struct {
	ConsulAddress      string
	SyncInterval       time.Duration
	HealthCheckTimeout time.Duration
	Context            context.Context
	Logger             zerolog.Logger

	lastSyncChecksum []byte
	serviceMap       map[string][]string
	serviceMapLock   *sync.Mutex
	rand             *rand.Rand
}

func NewConsulNSR(ctx context.Context, consulAddr string, syncInterval time.Duration, logger zerolog.Logger) *ConsulNSR {
	p := &ConsulNSR{
		ConsulAddress:      consulAddr,
		SyncInterval:       syncInterval,
		HealthCheckTimeout: time.Second * 10,
		Context:            ctx,
		Logger:             logger.With().Str("mod", "consul_nsr").Logger(),

		rand:           rand.New(rand.NewSource(time.Now().UnixNano())),
		serviceMapLock: &sync.Mutex{},
		serviceMap:     map[string][]string{},
	}

	go func() {
		for {
			logger.Info().Msg("starting watch loop")

			err := p.watch(p.Context)
			if err != nil {
				if err == ErrDone {
					break
				}

				logger.Error().Err(err).Msg("error in watch loop")
			}
		}
	}()

	go func() {
		for {
			logger.Info().Msg("starting healthcheck loop")

			err := p.runHealthChecks(p.Context)
			if err != nil {
				if err == ErrDone {
					break
				}

				logger.Error().Err(err).Msg("error in healthcheck loop")
			}
		}
	}()

	return p
}

func (p *ConsulNSR) Resolve(serviceName string) (string, bool) {
	p.Logger.Debug().Interface("service_map", p.serviceMap).Str("service_name", serviceName).Msg("resolve called")

	dsts, ok := p.serviceMap[serviceName]
	if !ok || len(dsts) == 0 {
		return "", false
	}

	if len(dsts) == 1 {
		p.Logger.Debug().
			Interface("service_map", p.serviceMap).
			Interface("dst", dsts[0]).Str("service_name", serviceName).
			Msg("resolved service to single available instance")

		return dsts[0], true
	}

	randomInt := p.rand.Intn(len(dsts))
	p.Logger.Debug().
		Interface("service_map", p.serviceMap).
		Interface("dst", dsts[randomInt]).Str("service_name", serviceName).
		Int("random_int", randomInt).
		Msg("resolved service to random available instance")

	return dsts[randomInt], true
}

func (p *ConsulNSR) Remove(serviceName, serviceAddress string) {
	p.Logger.Debug().
		Interface("service_map", p.serviceMap).
		Str("service_name", serviceName).
		Str("service_address", serviceAddress).
		Msg("removing service from registry")

	entry, ok := p.serviceMap[serviceName]
	if ok {
		if contains(entry, serviceAddress) {
			if len(entry) >= 1 {
				p.serviceMapLock.Lock()
				delete(p.serviceMap, serviceName)
				p.serviceMapLock.Unlock()
			} else {
				newEntry := make([]string, 0)
				for _, addr := range entry {
					if addr != serviceAddress {
						newEntry = append(newEntry, addr)
					}
				}

				p.serviceMapLock.Lock()
				p.serviceMap[serviceName] = newEntry
				p.serviceMapLock.Unlock()

				bytes, err := json.Marshal(p.serviceMap)
				if err != nil {
					p.Logger.Error().Err(err).Msg("failed to generate checksum for updated service")
					return
				}

				p.lastSyncChecksum = md5.New().Sum(bytes)
				p.Logger.Info().
					Interface("service_map", p.serviceMap).
					Str("service_name", serviceName).
					Str("service_address", serviceAddress).
					Msg("removed service from registry and updated sync checksum")
			}
		}
	}
}

func (p *ConsulNSR) runHealthChecks(ctx context.Context) error {
	p.Logger.Info().
		Dur("sync_interval", p.SyncInterval).
		Dur("timeout", p.HealthCheckTimeout).
		Msg("initializing healthchecker")

	ticker := time.NewTicker(p.SyncInterval)
	for {
		for serviceName, addresses := range p.serviceMap {
			for _, address := range addresses {
				err := catalog.AttemptTCPHealthCheck(address, p.HealthCheckTimeout)
				if err != nil {
					p.Logger.Error().
						Err(err).
						Str("serviceName", serviceName).
						Str("address", address).
						Msg("failed to complete healthcheck, removing service from map")

					p.Remove(serviceName, address)
				}

				// retries := 0
				// ticker := time.NewTicker(time.Second * 3)

				// for {
				// 	err := catalog.AttemptTCPHealthCheck(address, p.HealthCheckTimeout)
				// 	if err != nil {
				// 		if retries == 3 {
				// 			p.Logger.Error().
				// 				Err(err).
				// 				Str("serviceName", serviceName).
				// 				Str("address", address).
				// 				Msg("failed to complete healthcheck, removing service from map")

				// 			p.Remove(serviceName, address)
				// 			break
				// 		}

				// 		p.Logger.Debug().
				// 			Err(err).
				// 			Str("serviceName", serviceName).
				// 			Str("address", address).
				// 			Int("retry", retries).
				// 			Msg("failed to complete healthcheck, retrying")

				// 		<-ticker.C
				// 		retries++
				// 	} else {
				// 		break
				// 	}
			}
		}

		select {
		case <-ticker.C:
		case <-ctx.Done():
			ticker.Stop()
			return ErrDone
		}
	}
}

func (p *ConsulNSR) watch(ctx context.Context) error {
	p.Logger.Info().Dur("sync_interval", p.SyncInterval).Msg("initializing ticker")
	ticker := time.NewTicker(p.SyncInterval)

	p.Logger.Debug().Str("addr", p.ConsulAddress).Msg("dialing consul")
	client, err := catalog.NewClient(p.ConsulAddress)
	if err != nil {
		return err
	}

	for {
		services, err := client.FindLinkedServices()
		if err != nil {
			return err
		}

		p.Logger.Debug().Interface("services", services).Msg("fetched pre-filtered service list from Consul")
		updatedServiceMap := make(map[string][]string)

		for _, svc := range services {
			grpcServer, err := FromCatalog(svc)
			if err != ErrNotGRPCService {
				if err != nil {
					return err
				}

				grpcServices, err := reflector.ListGRPCServices(grpcServer.Host, grpc.WithBlock(), grpc.WithInsecure(), grpc.WithTimeout(time.Second*10))
				if err != nil {
					p.Logger.Error().Err(err).Interface("grpc_server", grpcServer).Msg("failed to fetch service list from GRPC server")
				}

				for _, grpcService := range grpcServices {
					// TODO check whitelist, check blacklist
					destinations, ok := updatedServiceMap[grpcService]
					if !ok {
						updatedServiceMap[grpcService] = make([]string, 0)
					}

					if !contains(destinations, grpcServer.Host) {
						updatedServiceMap[grpcService] = append(destinations, grpcServer.Host)
					}
				}
			}
		}

		bytes, err := json.Marshal(updatedServiceMap)
		if err != nil {
			return err
		}
		newHash := md5.New().Sum(bytes)

		p.Logger.Debug().Interface("service_map", updatedServiceMap).Msg("attempting update of internal service map")
		if p.lastSyncChecksum == nil || string(p.lastSyncChecksum) != string(newHash) {
			p.serviceMapLock.Lock()
			p.serviceMap = updatedServiceMap
			p.lastSyncChecksum = newHash
			p.serviceMapLock.Unlock()

			p.Logger.Debug().Interface("service_map", updatedServiceMap).Msg("updated internal service map")
		} else {
			p.Logger.Debug().Interface("service_map", updatedServiceMap).Msg("skipping update of service map, hashes map")
		}

		select {
		// block until next tick
		case <-ticker.C:
			p.Logger.Trace().Msg("next tick interval hit")
		// handle exit
		case <-ctx.Done():
			ticker.Stop()
			return ErrDone
		}
	}
}
