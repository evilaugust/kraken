package reflector

import (
	"context"
	"errors"

	"github.com/jhump/protoreflect/grpcreflect"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	rpb "google.golang.org/grpc/reflection/grpc_reflection_v1alpha"
	"google.golang.org/grpc/status"
)

// ErrNotSupported is used by ListGRPCServices when server reflection is not enabled.
var ErrNotSupported = errors.New("this grpc service is not supported (reflection service not available)")

// GRPCReflectionServiceFQN is the FQN of a grpc server reflection endpoint
const GRPCReflectionServiceFQN = "grpc.reflection.v1alpha.ServerReflection"

// ListGRPCServices dials a GRPC service and attempts to retrieve a list of
// services ([]string) using GRPC reflection. If the service doesn't have reflection
// enabled, an ErrNotSupported will be returned.
func ListGRPCServices(serviceAddress string, dialOptions ...grpc.DialOption) ([]string, error) {
	ctx := context.Background()
	dialOptions = append(dialOptions, grpc.WithBlock())
	dialOptions = append(dialOptions, grpc.WithMaxMsgSize(10*1024*1024))

	conn, err := grpc.Dial(serviceAddress, dialOptions...)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	stub := rpb.NewServerReflectionClient(conn)
	cli := grpcreflect.NewClient(ctx, stub)

	svcNames, err := cli.ListServices()
	if err != nil {
		if status.Code(err) == codes.Unimplemented {
			return nil, ErrNotSupported
		}

		return nil, err
	}

	serviceNames := make([]string, 0)
	for _, svcName := range svcNames {
		sd, err := cli.ResolveService(svcName)
		if err != nil {
			return nil, err
		}

		serviceName := sd.GetFullyQualifiedName()
		if serviceName != GRPCReflectionServiceFQN {
			serviceNames = append(serviceNames, serviceName)
		}
	}

	return serviceNames, nil
}
